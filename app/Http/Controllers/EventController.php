<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Calendar;
use App\Event;

class EventController extends Controller
{
    public function index()
    {
        $events = [];
        $data = Event::all();

        if($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = Calendar::event(
                    $value->title,
                    false,
                    new \DateTime($value->start_date),
                    new \DateTime($value->end_date),
                    null,
                    // Add color and link on event
	                [
	                    'color' => '#f05050',
	                    'url' => 'pass here url and any route',
	                ]
                );
            }
        }
        $calendar = Calendar::addEvents($events)->setOptions([
            //set fullcalendar options
            // 'header' => [
            //     "left" =>  "title",
            //     "center" => "",
            //     "right" => "month, listDay, listWeek, listMonth, agenda"
            // ],
            'nowIndicator' => true,
            'allDaySlot' => false,
            'minTime' => "08:00:00",
            'maxTime' => "20:00:00",
            'contentHeight' => "auto",
            'axisFormat' => 'H:mm',
        ]);
        return view('fullcalender', compact('calendar'));
    }
}
