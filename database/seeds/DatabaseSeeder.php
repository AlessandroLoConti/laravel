<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $data = [
        	['username'=>'Io', 'email'=>'m@', 'password'=>'sd'],
        	['username'=>'Mario', 'email'=>'mario@email.it', 'password'=>'sd'],
        ];
        foreach ($data as $key => $value) {
        	User::create($value);
        }
    }
}
