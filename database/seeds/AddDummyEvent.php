<?php

use Illuminate\Database\Seeder;
use App\Event;

class AddDummyEvent extends Seeder
{
    public function run()
    {
        $data = [
        	['title'=>'Demo Event-1', 'start_date'=>'2019-01-12T14:00:00', 'end_date'=>'2019-01-12T15:00:00'],
        	['title'=>'Demo Event-2', 'start_date'=>'2019-01-13T14:00:00', 'end_date'=>'2019-01-14T14:00:00'],
        	['title'=>'Demo Event-3', 'start_date'=>'2019-01-20T02:20:00', 'end_date'=>'2019-01-21T10:00:00'],
        ];
        foreach ($data as $key => $value) {
        	Event::create($value);
        }
    }
}
