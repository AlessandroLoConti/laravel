<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

Route::get('/', function () {
    return view('welcome');
});
Route::get('events', 'EventController@index');

Route::get('downloadcsv/{parameters}', function($parameters){
    if($parameters == 'events'){
        $result = \App\Event::all();
        $columms = ['title', 'start_date', 'end_date'];
    }else{
        $result = \App\User::all();
        $columms = ['username', 'email', 'id'];
    }
    $csvExporter = new \Laracsv\Export();
    return $csvExporter->build($result, $columms)->download('result.csv');
 });