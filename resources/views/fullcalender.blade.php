<!doctype html>

<html lang="en">
<head>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script>
function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
  </head>

<body>


<div class="dropdown">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
    Download Csv

  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="downloadcsv/events" download>Eventi</a>
    <a class="dropdown-item" href="downloadcsv/users" download>Utenti</a>
  </div>
</div>

<!--Campo di input password che richiede un formato particolare con una espressione regolare:
    -minimo 8 caratteri
    -minimo una lettera maiuscola
    -minimo una lettera minuscola
    -minimo un numero 0-9
    -->
<form >
  Password <input type="password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="min 1 lettera maiuscola, 1 numero">
  <input type="submit">
</form>

<br>
<div>

    <form oninput="txtFullName.value = txtFirstName.value +'.'+ txtLastName.value" >
        First name : <input type="text" name="txtFirstName"/> <br><br>
        Last name : <input type="text" name="txtLastName"/> <br><br>
        Genera username:<input type="text" name="txtFullName"> <br><br>

        <br>

<!-- Creazione di una password random di 8 caratteri con:
        -almeno 1 numero
        -almeno 1 lettera maiuscola
        -almeno 1 lettera maiuscola

-->
        <?php
            function random($length)
            {
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            $password = substr( str_shuffle(sha1(rand() . time()) . $chars ), 0, $length );
            return $password;
            }
            echo random(8);
        ?>
    </form>

</div>

<div class="container">
    <div class="panel panel-primary">
        <div class="panel-body" >
            {!! $calendar->calendar() !!}
            {!! $calendar->script() !!}
        </div>
    </div>
</div>
  <div>
    <!-- JQUERY SCRIPTED CAPTURE + PREVIEW -->
    <input type='file' accept="image/*" capture="camera" onchange="readURL(this);" />
    <img id="blah" style="height:30%; width: 40%;" src="http://placehold.it/180" alt="your image" />

  </div>
</body>

</html>
